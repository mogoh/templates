import Reveal from "/node_modules/reveal.js/dist/reveal.esm.js";
import RevealNotes from "/node_modules/reveal.js/plugin/notes/notes.esm.js";
import RevealMarkdown from "/node_modules/reveal.js/plugin/markdown/markdown.esm.js";
import RevealHighlight from "/node_modules/reveal.js/plugin/highlight/highlight.esm.js";
import RevealMath from "/node_modules/reveal.js/plugin/math/math.esm.js";

Reveal.initialize({
    hash: true,
    transition: 'fade',
    backgroundTransition: 'fade',
    center: false,

    // The "normal" size of the presentation, aspect ratio will
    // be preserved when the presentation is scaled to fit different
    // resolutions. Can be specified using percentage units.
    width: 1920,
    height: 1080,

    // Factor of the display size that should remain empty around
    // the content
    margin: 0.05,

    // Bounds for smallest/largest possible scale to apply to content
    minScale: 0.1,
    maxScale: 0.9,
    // disableLayout: true,

    // Enable slide navigation via mouse wheel
    mouseWheel: true,

    // Learn about plugins: https://revealjs.com/plugins/
    plugins: [
        RevealMarkdown,
        RevealHighlight,
        RevealNotes,
        RevealMath,
    ],
});