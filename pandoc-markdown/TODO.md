## To Do

- https://print-css.rocks/

## Vivliostyle

- update viviliostyle
- https://vivliostyle.org/

## Weasyprint

$`weasyprint --stylesheet ${css} ${infile} ${outfile}`

## Pagedjs

https://pagedjs.org/

$`pagedjs-cli ${css} ${infile} --output ${outfile}`

## Math

Math does not work.
Pandoc lua filters are old and mathjax is even older.
If this does not get update, we have no chance:

- https://github.com/pandoc/lua-filters/tree/master/math2svg
- https://github.com/mathjax/mathjax-node-cli
