---
lang: en-Us
title:  'Pandoc Markdown: Examples'
toc-title: 'Inhaltsverzeichnis'
subtitle:
    A short collection of markdown examples for the pandoc parser.
author:
- Leonhard Küper
keywords: [nothing, nothingness]
abstract: |
  This is the abstract.

  It consists of two paragraphs.
subject:
    document subject, included in ODT, PDF, docx and pptx metadata 
description:
    document description, included in ODT, docx and pptx metadata. Some applications show this as Comments metadata. 
category:
    document category, included in docx and pptx metadata
---
## Blindtext

The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! "Now fax quiz Jack!" my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV for luck. A wizard’s job is to vex chumps quickly in fog. Watch "Jeopardy!", Alex Trebek's fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just

aäb cde fgh ijk lmn oöp qrsß tuü vwx yz AÄBC DEF GHI JKL MNO ÖPQ RSẞT UÜV WXYZ !"§ $%& /() =?* '<> #|; ²³~ @`´ ©«» ¼× {} „“” ‚‘’ -–— +

## Headings

### A level-three heading ###

#### A level-four heading

##### A level-five heading

###### A level-six heading

### A level-three heading with a [link](/url) and *emphasis*

I like several of their flavors of ice cream:
#22, for example, and #5.

### My heading {#foo}

### My heading ###    {#bar}

### My heading {-}

### My heading {.unnumbered}

### Heading identifiers in HTML

[Heading identifiers in HTML]

[Heading identifiers in HTML][]

[the section on heading identifiers][heading identifiers in HTML]

### BarFoo

[foo]: bar

See [foo]

## Block quotations

> This is a block quote. This
> paragraph has two lines.
>
> 1. This is a list inside a block quote.
> 2. Second item.

> This is a block quote. This
paragraph has two lines.

> 1. This is a list inside a block quote.
2. Second item.

> This is a block quote.
>
> > A block quote within a block quote.

>     code

> This is a block quote.
>> Nested.

    if (a > 3) {
      moveShip(5 * gravity, DOWN);
    }

## Fenced code blocks

~~~~~~~
if (a > 3) {
  moveShip(5 * gravity, DOWN);
}
~~~~~~~

~~~~~~~~~~~~~~~~
~~~~~~~~~~
code including tildes
~~~~~~~~~~
~~~~~~~~~~~~~~~~

~~~~ {#mycode .haskell .numberLines startFrom="100"}
qsort []     = []
qsort (x:xs) = qsort (filter (< x) xs) ++ [x] ++
               qsort (filter (>= x) xs)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

```haskell
qsort [] = []
```

``` {.haskell}
qsort [] = []
```

```
0123456789
01234567890123456789
012345678901234567890123456789
0123456789012345678901234567890123456789
01234567890123456789012345678901234567890123456789
012345678901234567890123456789012345678901234567890123456789
0123456789012345678901234567890123456789012345678901234567890123456789
01234567890123456789012345678901234567890123456789012345678901234567890123456789
```

## Line blocks

| The limerick packs laughs anatomical
| In space that is quite economical.
|    But the good ones I've seen
|    So seldom are clean
| And the clean ones so seldom are comical

| 200 Main St.
| Berkeley, CA 94718

| The Right Honorable Most Venerable and Righteous Samuel L.
  Constable, Jr.
| 200 Main St.
| Berkeley, CA 94718

## Lists

* one
* two
* three

* one

* two

* three

* here is my first
  list item.
* and my second.

* here is my first
list item.
* and my second.


  * First paragraph.

    Continued.

  * Second paragraph. With a code block, which must be indented
    eight spaces:

        { code }

*     code

  continuation paragraph

* fruits
  + apples
    - macintosh
    - red delicious
  + pears
  + peaches
* vegetables
  + broccoli
  + chard

## Ordered lists

1.  one
2.  two
3.  three

5.  one
7.  two
1.  three

#. one
#. two

 9)  Ninth
10)  Tenth
11)  Eleventh
       i. subone
      ii. subtwo
     iii. subthree

(2) Two
(5) Three
1.  Four
*   Five

#.  one
#.  two
#.  three

- [ ] an unchecked task list item
- [x] checked item

## Definition lists

Term 1

:   Definition 1

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

Term 1

:   Definition
with lazy continuation.

    Second paragraph of the definition.

Term 1
  ~ Definition 1

Term 2
  ~ Definition 2a
  ~ Definition 2b

## Numbered example lists

(@)  My first example will be numbered (1).
(@)  My second example will be numbered (2).

Explanation of examples.

(@)  My third example will be numbered (3).

(@good)  This is a good example.

As (@good) illustrates, ...

## Ending a list

-   item one
-   item two

<!-- end of list -->

    { my code block }

1.  one
2.  two
3.  three

<!-- -->

1.  uno
2.  dos
3.  tres

## Horizontal rules

*  *  *  *

---------------

## Tables

### Simple Tables

  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table:  Demonstration of simple table syntax.

-------     ------ ----------   -------
     12     12        12             12
    123     123       123           123
      1     1          1              1
-------     ------ ----------   -------

### Multiline Tables

-------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows.
-------------------------------------------------------------

Table: Here's the caption. It, too, may span
multiple lines.

----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows.
----------- ------- --------------- -------------------------

: Here's a multiline table without a header.

### Grid Tables

: Sample grid table.

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+

+---------------+---------------+--------------------+
| Right         | Left          | Centered           |
+==============:+:==============+:==================:+
| Bananas       | $1.34         | built-in wrapper   |
+---------------+---------------+--------------------+

+--------------:+:--------------+:------------------:+
| Right         | Left          | Centered           |
+---------------+---------------+--------------------+

+-----------------------+------------------------------------------------------+
| Somthing              | Something                                            |
+-----------------------+------------------------------------------------------+

### Pipe Tables

| Right | Left | Default | Center |
|------:|:-----|---------|:------:|
|   12  |  12  |    12   |    12  |
|  123  |  123 |   123   |   123  |
|    1  |    1 |     1   |     1  |

  : Demonstration of pipe table syntax.

## Backslash escapes

*\*hello\**

Nonbreaking\ space.

New\
line.

## Emphasis

This text is _emphasized with underscores_, and this
is *emphasized with asterisks*.

This is **strong emphasis** and __with underscores__.

This is * not emphasized *, and \*neither is this\*.

feas*ible*, not feas*able*.

## Strikeout

This ~~is deleted text.~~

## Superscripts and subscripts

H~2~O is a liquid.  2^10^ is 1024.

## Verbatim

What is the difference between `>>=` and `>>`?

Here is a literal backtick `` ` ``.

This is a backslash followed by an asterisk: `\*`.

`<$>`{.haskell}

## Small caps

[Small caps]{.smallcaps}

<span class="smallcaps">Small caps</span>

<span style="font-variant:small-caps;">Small caps</span>

## Math

$a+b=c$

$$a + b = c$$

$$\frac{1}{2}$$

$a^2 + b^2 = c^2$

$v(t) = v_0 + \frac{1}{2}at^2$

$\gamma = \frac{1}{\sqrt{1 - v^2/c^2}}$

$\exists x \forall y (Rxy \equiv Ryx)$

$p \wedge q \models p$

$\Box\diamond p\equiv\diamond p$

$\int_{0}^{1} x dx = \left[ \frac{1}{2}x^2 \right]_{0}^{1} = \frac{1}{2}$

$e^x = \sum_{n=0}^\infty \frac{x^n}{n!} = \lim_{n\rightarrow\infty} (1+x/n)^n$

$$
\begin{matrix}
w(p) &=& 1÷(2-p)      &, \text{wenn}\ p < 0\\
w(p) &=& 1÷2          &, \text{wenn}\ p = 0\\
w(p) &=& (1+p)÷(2+p)  &, \text{wenn}\ p > 0
\end{matrix}
$$

## Links

<http://google.com>
<sam@green.eggs.ham>

This is an [inline link](/url), and here's [one with
a title](http://fsf.org "click here for a good time!").

[Write me!](mailto:sam@green.eggs.ham)

[my label 1]: /foo/bar.html  "My title, optional"
[my label 2]: /foo
[my label 3]: http://fsf.org (The free software foundation)
[my label 4]: /bar#special  'A title in single quotes'

[my label 5]: <http://foo.bar.baz>

[my label 3]: http://fsf.org
  "The free software foundation"

Here is [my link][FOO]

[Foo2]: /bar/baz

See [my website][].

[my website]: http://foo.bar.baz

See [my website].

[my website]: http://foo.bar.baz

See the [Introduction](#introduction).

See the [Introduction].

[Introduction]: #introduction

## Images

![la lune](./images/Woman's Home Companion 1938-11.jpg "Voyage to the moon")

![movie reel]

[movie reel]: ./images/Rotating_earth_(large).gif

![This is the caption](./images/png-logo.png)

![This image won't be a figure](./images/png-logo.png)\

An inline ![image](./images/Woman's Home Companion 1938-11.jpg){#id .class width=30 height=20px}
and a reference ![image][ref] with attributes.

[ref]: ./images/Woman's Home Companion 1938-11.jpg "optional title" {#id .class key=val key2="val 2"}

![](./images/Woman's Home Companion 1938-11.jpg){ width=50% }

## Divs and Spans

::::: {#special .box}
Here is a paragraph.

And another.
:::::

::: Warning ::::::
This is a warning.

::: Danger
This is a warning within a warning.
:::
::::::::::::::::::

[This is *some text*]{.class key="val"}

## Footnotes

Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.

[^longnote]: Here's one with multiple blocks.

    Subsequent paragraphs are indented to show that they
belong to the previous footnote.

        { some.code }

    The whole paragraph can be indented, or just the first
    line.  In this way, multi-paragraph footnotes work like
    multi-paragraph list items.

This paragraph won't be part of the note, because it
isn't indented.

Here is an inline note.^[Inlines notes are easier to write, since
you don't have to pick an identifier and move down to type the
note.]