#!/usr/bin/env zx
import { watch } from "node:fs/promises";
import { join } from "node:path";

async function cwd() {
    let dir = String(await $`pwd`);
    return dir.replace(/\n$/, "");
}

async function copy_data(config) {
    try {
        await fs.copy(`${config.css_dir}/js`, `${config.dest_dir}/js`);
        console.log("Copied JS.");
    } catch (err) {
        console.error(err);
    }
    try {
        await fs.copy(`${config.css_dir}/css`, `${config.dest_dir}/css`);
        console.log("Copied CSS.");
    } catch (err) {
        console.error(err);
    }
    try {
        await fs.copy(`${config.src_dir}/images`, `${config.dest_dir}/images`);
        console.log("Copied Images.");
    } catch (err) {
        console.error("WARNING: Copying images failed.");
    }
}

async function clean(config) {
    async function removeAll(files) {
        for (let file of await glob(files)) {
            await fs.remove(file);
        }
    }
    await fs.remove(`${config.dest_dir}/css`);
    await fs.remove(`${config.dest_dir}/images`);
    await fs.remove(`${config.dest_dir}/js`);
    removeAll(`${config.dest_dir}/*.html`);
    removeAll(`${config.dest_dir}/*.epub`);
    removeAll(`${config.dest_dir}/*.pdf`);
}

async function md_to_html(config, outfile) {
    let flags = [
        "--verbose",
        "--from=markdown",
        "--to=html",
        "--standalone",
        "--css=css/index.css",
        `--include-in-header=${config.templates_dir}/header-includes.html`,
        `--include-before-body=${config.templates_dir}/body-includes.html`,
        `--template=${config.templates_dir}/template.html`,
        "--toc",
        "--mathml",
        `--output=${outfile}`,
        ...config.full_src_files,
    ];
    return $`pandoc ${flags}`;
}

async function md_to_epub(config, outfile) {
    let flags = [
        "--verbose",
        "--from=markdown",
        "--to=epub",
        `--resource-path=${config.dest_dir}:${config.src_dir}`,
        "--standalone",
        // "--webtex",
        // `--css=${config.dest_dir}/css/index.css`,
        // `--include-in-header=${config.templates_dir}/header-includes.html`,
        // `--include-before-body=${config.templates_dir}/body-includes.html`,
        // `--template=${config.templates_dir}/template.html`,
        "--toc",
        "--mathml",
        `--output=${outfile}`,
        ...config.full_src_files,
    ];
    return $`pandoc ${flags}`;
}

async function html_to_pdf_vivliostyle(config, outfile, additional_paramters) {
    let flags = [
        "build",
        config.html_file,
        "--output",
        outfile,
        "--log-level",
        "verbose",
    ].concat(additional_paramters);
    return $`vivliostyle ${flags}`;
}

async function server(config) {
    let flags = [
        "start",
        "--files",
        ".css",
        "--files",
        ".js",
        "--files",
        ".html",
        "--watch",
        `--server`,
        config.dest_dir,
        "--port",
        "8080",
        "--no-open",
        "--no-notify",
        // `--index`, `example.html`,
    ];
    return $`browser-sync ${flags}`;
}

async function build(config) {
    // Copy
    copy_data(config);

    let html_file = `${config.dest_dir}/${config.name}.html`;
    let pdf_a4_file = `${config.dest_dir}/${config.name}_a4.pdf`;
    let pdf_a5_file = `${config.dest_dir}/${config.name}_a5.pdf`;
    let pdf_a6_file = `${config.dest_dir}/${config.name}_a6.pdf`;
    let pdf_a7_file = `${config.dest_dir}/${config.name}_a7.pdf`;
    let epub_file = `${config.dest_dir}/${config.name}.epub`;

    // md -> html
    if (argv.html || argv.t) {
        await md_to_html(config, html_file);
    }

    // md -> epub
    if (argv.epub || argv.e) {
        await md_to_epub(config, epub_file);
    }

    // html -> pdf (vivlio)
    if (argv.pdfa4 || argv.p4) {
        html_to_pdf_vivliostyle(config, pdf_a4_file, ["--size", "A4"]);
    }
    if (argv.pdfa5 || argv.p5) {
        html_to_pdf_vivliostyle(config, pdf_a5_file, ["--size", "A5"]);
    }
    if (argv.pdfa6 || argv.p6) {
        html_to_pdf_vivliostyle(config, pdf_a6_file, ["--size", "A6"]);
    }
    if (argv.pdfa7 || argv.p7) {
        html_to_pdf_vivliostyle(config, pdf_a7_file, ["--size", "A7"]);
    }
}

async function main() {
    const ZERO_ARGUMENTS =
        Object.keys(argv).length === 1 && argv["_"].length === 0;
    if (argv.h || argv.help || ZERO_ARGUMENTS) {
        echo(`
Usage: markdown-converter.mjs [SRC-FILES...] --FLAGS

SRC-FILES:
    Input files must be .md-files, provided as a list.

ENVIRONMENT VARIABLES:
    SRC_DIR="/path/to/src"
    SRC_FILES="list.md/of.md/files.md"
    TEMPLATES_DIR="/path/to/templates"
    CSS_DIR="/path/to/css/dir"
    DEST_DIR="/path/to/out"
    OUTPUT="name"

FLAGS:
    --srcdir=/path/to/src
    --templatesdir=/path/to/templates
    --cssdir=/path/to/css/dir
    --destdir=/path/to/out
    --output=name
    --clean, -c     Clean the output directory.
    --html, -t      Translate the Markdown to HTML with Pandoc.
    --pdfa4, --p4   Translate the HTML to PDF with vivliostyle.
    --pdfa5, --p5   Translate the HTML to PDF with vivliostyle.
    --pdfa6, --p6   Translate the HTML to PDF with vivliostyle.
    --pdfa7, --p7   Translate the HTML to PDF with vivliostyle.
    --epub, -e      Translate the Markdown to epub with Pandoc.
    --serve, -s     Start a dev server.
    --build, -b     Translate.
    --watch, -w     Watch for file changes and rerun translation.
`);
        process.exit(0);
    }

    // Generating Config

    let config = {};

    if (argv.srcdir !== undefined) {
        config.src_dir = argv.srcdir;
    } else if (process.env.SRC_DIR !== undefined) {
        config.src_dir = process.env.SRC_DIR;
    } else {
        console.log("WARNING: src dir is not set.");
        config.src_dir = join(await cwd(), "/src");
    }

    if (argv.output !== undefined) {
        config.name = argv.output;
    } else if (process.env.OUTPUT !== undefined) {
        config.name = process.env.OUTPUT;
    } else {
        console.log("WARNING: output is not set.");
        config.name = "output";
    }

    if (argv["_"].length > 0) {
        config.src_files = argv["_"];
    } else if (process.env.SRC_FILES !== undefined) {
        config.src_files = process.env.SRC_FILES.split("/");
    } else {
        console.log("WARNING: src file(s) not set.");
        config.src_files = await glob("*.md", { cwd: config.src_dir });
    }
    config.full_src_files = config.src_files.map(
        (file) => `${config.src_dir}/${file}`
    );

    if (argv.destdir !== undefined) {
        config.dest_dir = argv.destdir;
    } else if (process.env.DEST_DIR !== undefined) {
        config.dest_dir = process.env.DEST_DIR;
    } else {
        console.log("WARNING: dest dir is not set.");
        config.dest_dir = join(await cwd(), "/out");
    }

    if (argv.cssdir !== undefined) {
        config.css_dir = argv.cssdir;
    } else if (process.env.CSS_DIR !== undefined) {
        config.css_dir = process.env.CSS_DIR;
    } else {
        console.log("WARNING: css dir is not set.");
        config.css_dir = join(await cwd(), "/css");
    }

    if (argv.templatesdir !== undefined) {
        config.templates_dir = argv.templatesdir;
    } else if (process.env.TEMPLATES_DIR !== undefined) {
        config.templates_dir = process.env.TEMPLATES_DIR;
    } else {
        console.log("WARNING: templates dir is not set.");
        config.templates_dir = join(await cwd(), "/templates");
    }

    config.html_file = `${config.dest_dir}/${config.name}.html`;
    config.pdf_a4_file = `${config.dest_dir}/${config.name}_a4.pdf`;
    config.pdf_a5_file = `${config.dest_dir}/${config.name}_a5.pdf`;
    config.pdf_a6_file = `${config.dest_dir}/${config.name}_a6.pdf`;
    config.pdf_a7_file = `${config.dest_dir}/${config.name}_a7.pdf`;
    config.epub_file = `${config.dest_dir}/${config.name}.epub`;

    // End Generating Config

    if (argv.clean || argv.c) {
        await clean(config);
    }

    if (argv.serve || argv.s) {
        server(config);
    }

    if (argv.watch || argv.w) {
        build(config);
        try {
            const watcher = watch(config.src_dir, { recursive: true });
            for await (const event of watcher) {
                console.log(event);
                build(config);
            }
        } catch (err) {
            if (err.name === "AbortError") {
                return;
            }
            throw err;
        }
    }

    if ((argv.build || argv.b) && !(argv.watch || argv.w)) {
        build(config);
    }
}

main();
