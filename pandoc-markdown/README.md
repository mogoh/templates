# README

## Requirements

Use this container: https://gitlab.com/mogoh/tools/-/tree/main/containers/converter?ref_type=heads

Distrobox: https://github.com/89luca89/distrobox

## Setup .env

```bash
cp .env.example .env
editor .env
```

## Usage

Usage: markdown-converter.mjs [SRC-FILES...] --FLAGS

SRC-FILES:
    Input files must be .md-files, provided as a list.

ENVIRONMENT VARIABLES:
    SRC_DIR="/path/to/src"
    SRC_FILES="list.md/of.md/files.md"
    TEMPLATES_DIR="/path/to/templates"
    CSS_DIR="/path/to/css/dir"
    DEST_DIR="/path/to/out"
    OUTPUT="name"

FLAGS:
    --srcdir=/path/to/src
    --templatesdir=/path/to/templates
    --cssdir=/path/to/css/dir
    --destdir=/path/to/out
    --output=name
    --clean, -c     Clean the output directory.
    --html, -t      Translate the Markdown to HTML with Pandoc.
    --pdfa4, --p4   Translate the HTML to PDF with vivliostyle.
    --pdfa5, --p5   Translate the HTML to PDF with vivliostyle.
    --pdfa6, --p6   Translate the HTML to PDF with vivliostyle.
    --pdfa7, --p7   Translate the HTML to PDF with vivliostyle.
    --epub, -e      Translate the Markdown to epub with Pandoc.
    --serve, -s     Start a dev server.
    --build, -b     Translate.
    --watch, -w     Watch for file changes and rerun translation.

## Examples

```bash
. .env
# Only html
markdown-converter.mjs example.md --build --html
# Everything
markdown-converter.mjs example.md --build --html --pdfa4 --pdfa5 --pdfa6 --pdfa7 --epub
# Watchmode
markdown-converter.mjs example.md --watch
# Serve
markdown-converter.mjs example.md --serve
# HTML, serve, watch
markdown-converter.mjs example.md -tesw
```
