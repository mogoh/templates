import typescript from "@rollup/plugin-typescript";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import multiInput from "rollup-plugin-multi-input";


const config = {
    frontend: {
        input: ["src/client/js/**/*.ts"],
        output: {
            // file: "dist/client/js/index.js",
            format: "esm",
            dir: "dist/",
            sourcemap: "inline",
        },
        plugins: [
            typescript(),
            nodeResolve({ preferBuiltins: false }),
            commonjs(),
            json(),
            multiInput(),
        ],
    },
    backend: {
        input: "src/server/server.ts",
        output: {
            file: "dist/server/server.js",
            format: "esm",
            sourcemap: "inline",
        },
        plugins: [
            typescript(),
        ],
    },
};

const PART = process.env.PART;

export default config[PART];
