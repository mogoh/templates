import postcssPresetEnv from "postcss-preset-env";
import atImport from "postcss-import";
import importUrl from "postcss-import-url";
import cssnano from "cssnano";
import url from "postcss-url";


let config = {
    plugins: [
        importUrl(),
        atImport(),
        postcssPresetEnv({
            stage: 0,
        }),
        cssnano({
            preset: "default",
        }),
        url({
            url: "copy",
            assetsPath: ".",
        }),
    ],
};

export default config;