curl --request POST \
     --url http://localhost:3000/api/user/create_account \
     --header "Content-Type: application/json" \
     --data '{"username": "testuser", "password": "testpassword"}' \
     --include --verbose

curl --request POST \
     --url http://localhost:3000/api/user/login \
     --header "Content-Type: application/json" \
     --data '{"username": "testuser", "password": "testpassword"}' \
     --include --verbose
