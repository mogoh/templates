# To Do

- remove all podman and move to docker
- simple user interface
- do remaining controller
- password salt + hash
- sassion management
- user profile pages

## Deployment

### Gitlab

- build the container in a gitlab runner

### nginx

- https://fastify.dev/docs/latest/Guides/Recommendations/#nginx
- SSL/TLS

### Docker

- Setup docker for deployment
