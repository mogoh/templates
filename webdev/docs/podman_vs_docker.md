# Podman or Docker?

Podman creates some problems and it is probably not there yet.
Docker-compose has some issues with podman: https://github.com/containers/podman/issues/15580
So podman needs to run with some self made starting scripts.
Best for now is to use Docker instead.


## Podman and Ports

Podman runs rootless, but it might need access to port 80.
[Having priviliged ports might not be sensable anymore anyway.](https://ar.al/2022/08/30/dear-linux-privileged-ports-must-die/)

Drop privileged ports with:

```bash
sudo sysctl net.ipv4.ip_unprivileged_port_start=80
# Make it persistent:
echo "net.ipv4.ip_unprivileged_port_start=80" | sudo tee "/etc/sysctl.d/99-reduce-unprivileged-port-start-to-80.conf"
```

## Podman on Fedora and SELinux

Fedora comes preconfigured with relative strict SELinux policies.
To be able for containers to read volumes, they need do be mounted with `:z`.
If you need to be sure, that other containers are not able to read volumes, things get a bit more complicated.
See: https://blog.christophersmart.com/2021/01/31/podman-volumes-and-selinux/

## Running Docker Compose with Rootless Podman

See: https://fedoramagazine.org/use-docker-compose-with-podman-to-orchestrate-containers-on-fedora/

```bash
systemctl --user enable podman.socket
systemctl --user start podman.socket
systemctl --user status podman.socket
export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock
```

```bash
export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock
echo "export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock" >> $HOME/.bashrc
```

## podman compose

```bash
cat <<EOF > ${XDG_CONFIG_HOME}/containers/containers.conf
compose_providers=["podman-compose"]
compose_warning_logs=false
EOF
```
