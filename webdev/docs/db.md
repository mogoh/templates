We use drizzle orm.
https://orm.drizzle.team/

### Generate Migrations:

```bash
npx drizzle-kit generate --config=./src/server/db/drizzle.config.ts --name=initial
```

### Migrate:

```bash
npx drizzle-kit migrate --config=./src/server/db/drizzle.config.ts
```
