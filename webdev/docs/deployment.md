## podman build container image for production

```bash
. bin/load_env
bin/container_build.mjs
```

### podman start container

```bash
. bin/load_env
"${CONTAINER_TOOL}" run "${IMAGE_NAME}"
```

### podman enter a non-running container

```bash
. bin/load_env
"${CONTAINER_TOOL}" run --interactive --tty --entrypoint bash "${IMAGE_NAME}"
```

### podman enter a running container

```bash
. bin/load_env
"${CONTAINER_TOOL}" exec --interactive --tty "${CONTAINER_NAME}" bash
```

### podman stop all and remove all

```bash
. bin/load_env
bin/container_clean_up
```

## Deployment and production commands

- Be sure that you have configured your environment.

```bash
. bin/load_env
"${CONTAINER_TOOL}" login --password "${REGISTRY_PASSWORD}" --username "${REGISTRY_USER}" "${REGISTRY}"
```

```bash
. bin/load_env
"${CONTAINER_TOOL}" compose --file container/compose.yaml pull
"${CONTAINER_TOOL}" compose --file container/compose.yaml up
"${CONTAINER_TOOL}" compose --file container/compose.yaml up --detach
"${CONTAINER_TOOL}" compose --file container/compose.yaml down
```
