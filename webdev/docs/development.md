## Development commands

### Development preparations

```bash
cp .env.example .env
editor .env
source .env
yarn install
```

### Development

```bash
yarn dev
yarn eslint
```

### Clean up

```bash
rm -rf build
```
