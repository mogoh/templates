import postcssPresetEnv from "postcss-preset-env";
import url from "postcss-url";


let config = {
    plugins: [
        postcssPresetEnv({
            stage: 0,
        }),
        url({
            url: "copy",
            assetsPath: ".",
        }),
    ],
};

export default config;