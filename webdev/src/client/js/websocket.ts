// Define a WebSocket URL
const websocketUrl: string = "ws://localhost:3000/ws";

// Create a WebSocket connection
const socket: WebSocket = new WebSocket(websocketUrl);

// Event: Connection opened
socket.addEventListener("open", (event): void => {
    console.log("Connected to the WebSocket server.");

    // Send a message to the server
    const message: string = "Hello, WebSocket server!";
    socket.send(message);
    console.log(`Message sent: ${message}`);
});

// Event: Message received from server
socket.addEventListener("message", (event: MessageEvent): void => {
    const serverMessage: string = event.data as string;
    console.log(`Message from server: ${serverMessage}`);
});

// Event: Connection closed
socket.addEventListener("close", (event: CloseEvent): void => {
    console.log(`Disconnected from the WebSocket server. Code: ${event.code}, Reason: ${event.reason}`);
});

// Event: Error occurred
socket.addEventListener("error", (event: Event): void => {
    console.error("WebSocket error:", event);
});
