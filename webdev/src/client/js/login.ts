/* eslint-disable @typescript-eslint/no-misused-promises */
const login_form = document.getElementById("login_form") as HTMLFormElement;

interface LoginResponse {
    success: boolean;
    token?: string;
    message?: string;
}

async function listener(e: Event): Promise<void> {
    e.preventDefault();

    const form_data = new FormData(login_form);
    const body = JSON.stringify(Object.fromEntries(form_data.entries()));

    try {
        const response = await fetch("/api/user/login", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body,
        });

        const result: LoginResponse = await response.json() as LoginResponse;
        console.log(result);
    } catch (error) {
        console.error("Login failed", error);
    }
}

login_form.addEventListener("submit", listener);
