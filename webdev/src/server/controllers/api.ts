import { FastifyReply, FastifyRequest } from "fastify";
import { eq } from "drizzle-orm";

import { db } from "../server.js";
import { users } from "../db/schema.js";


export async function create_user(request: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> {
    const { username, password } = request.body as { username: string, password: string };

    // Validate inputs
    if (!username || !password) {
        return reply.status(400).send({ message: "Username and password are required" });
    }

    try {
        // Insert user into the database
        await db.insert(users).values({
            username: username,
            password: password,
        });

        return await reply.status(201).send({ message: "User created successfully" });
    } catch (err) {
        const errorMessage = err instanceof Error ? err.message : "Unknown error";
        return reply.status(500).send({ message: "Error creating user", error: errorMessage });
    }

}

export async function login(request: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> {
    const { username, password } = request.body as { username: string, password: string };

    // Validate inputs
    if (!username || !password) {
        return reply.status(400).send({ message: "Username and password are required" });
    }

    try {
        // Retrieve the user from the database
        const user = await db.select().from(users).where(eq(users.username, username)).limit(1).execute();

        if (user.length === 0) {
            return await reply.status(404).send({ message: "Login unsuccessful" });
        }

        // Compare the password
        const is_valid_password = password === user[0].password;

        if (!is_valid_password) {
            return await reply.status(401).send({ message: "Login unsuccessful" });
        }

        // Successful login
        return await reply.send({ message: "Login successful" });
    } catch (err) {
        const errorMessage = err instanceof Error ? err.message : "Unknown error";
        return reply.status(500).send({ message: "Error logging in", error: errorMessage });
    }
}

export async function logout(request: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> {
    //todo
    return await reply.send();
}

export async function change_password(request: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> {
    //todo
    return await reply.send();

}

export async function change_user_data(request: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> {
    //todo
    return await reply.send();

}

export async function delete_user(request: FastifyRequest, reply: FastifyReply): Promise<FastifyReply> {
    //todo
    return await reply.send();
}

