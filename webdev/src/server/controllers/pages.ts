import { FastifyReply, FastifyRequest } from "fastify";


export function login_page_handler(request: FastifyRequest, reply: FastifyReply): FastifyReply {
    return reply.sendFile("login.html");
}
