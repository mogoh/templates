import { FastifyRequest, FastifyInstance, FastifyPluginOptions } from "fastify";
import WebSocket, { } from "@fastify/websocket";


export function websocket_plugin(fastify: FastifyInstance, options: FastifyPluginOptions): void {
    fastify.get(
        "/ws",
        { websocket: true },
        (socket: WebSocket.WebSocket, request: FastifyRequest) => {
            console.log("Client connected");

            // Listen for messages from the client
            socket.on("message", (message) => {
                console.log("Received:", message.toString());

                // Echo the message back to the client
                socket.send(`Echo: ${message}`);
            });

            // Handle client disconnection
            socket.on("close", () => {
                console.log("Client disconnected");
            });
        },
    );
}