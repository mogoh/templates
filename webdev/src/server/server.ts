import WebSocket from "@fastify/websocket";
import fastifyStatic from "@fastify/static";
import { fastify as Fastify, FastifyInstance } from "fastify";

import * as config from "./config.js";
import { routes } from "./routes.js";
import { websocket_plugin } from "./websocket.js";
import { drizzle, NodePgDatabase } from "drizzle-orm/node-postgres";

// Create instance
const fastify: FastifyInstance = Fastify({
    logger: true
});

// Create a Database connection
if (!config.DATABASE_URL) {
    throw new Error("DATABASE_URL is not defined");
}
export const db: NodePgDatabase = drizzle(config.DATABASE_URL);

// Register the static files plugin
fastify.register(fastifyStatic, {
    root: config.PUBLIC,
});

// Register the WebSocket plugin
fastify.register(WebSocket);
fastify.register(websocket_plugin);

// Register routes
fastify.register(routes);

// Print routes and plugins
fastify.ready().then(() => {
    console.log(fastify.printRoutes());
    console.log(fastify.printPlugins());
});

// Run the server!
try {
    await fastify.listen({ host: config.HOST, port: config.PORT });
} catch (err) {
    fastify.log.error(err);
    process.exit(1);
}