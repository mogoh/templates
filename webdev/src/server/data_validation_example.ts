import { FastifyInstance, FastifyPluginOptions, FastifyReply, FastifyRequest, RouteShorthandOptions } from "fastify";


const opts: RouteShorthandOptions = {
    schema: {
        body: {
            type: "object",
            properties: {
                someKey: { type: "string" },
                someOtherKey: { type: "number" }
            }
        }
    }
};

export function routes(fastify: FastifyInstance, options: FastifyPluginOptions): void {
    fastify.post("/", opts, async (request: FastifyRequest, reply: FastifyReply) => {
        return { hello: "world" };
    });
}
