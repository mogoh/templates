import { FastifyInstance, FastifyPluginOptions } from "fastify";
import {
    change_password,
    change_user_data,
    create_user,
    delete_user,
    login,
    logout
} from "./controllers/api.js";
import { login_page_handler } from "./controllers/pages.js";

/**
 * Encapsulates the routes
 */
export function routes(fastify: FastifyInstance, options: FastifyPluginOptions): void {
    fastify.post("/api/user/create_account", create_user);
    fastify.post("/api/user/delete_account", delete_user);
    fastify.post("/api/user/login", login);
    fastify.post("/api/user/logout", logout);
    fastify.post("/api/user/update_data", change_user_data);
    fastify.post("/api/user/update_password", change_password);

    fastify.get("/login", login_page_handler);
}
