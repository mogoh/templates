import path from "path";
import { fileURLToPath } from "url";

export const PORT = 80;
export const DIR_NAME = path.dirname(fileURLToPath(import.meta.url));
export const PUBLIC = path.join(DIR_NAME, "../client");
export const HOST = "::";
export const DATABASE_URL = process.env.DATABASE_URL;
