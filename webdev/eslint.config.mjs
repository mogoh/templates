import globals from "globals";
import js from "@eslint/js";
import tseslint from 'typescript-eslint';


export default [
    js.configs.recommended,
    ...tseslint.configs.recommendedTypeChecked,
    ...tseslint.configs.strictTypeChecked,
    ...tseslint.configs.stylisticTypeChecked,
    {
        files: [
            "src/**/*.js",
            "src/**/*.mjs",
            "src/**/*.ts",
        ],
        languageOptions: {
            ecmaVersion: 2022,
            sourceType: "module",
            globals: {
                ...globals.browser,
                ...globals.node,
            },
            parserOptions: {
                projectService: true,
                tsconfigRootDir: import.meta.dirname,
            },
        },
        rules: {
            indent: ["warn", 4],
            "linebreak-style": ["warn", "unix"],
            quotes: ["warn", "double"],
            "brace-style": [
                "warn",
                "1tbs",
                {
                    allowSingleLine: true,
                },
            ],
            "default-case": "warn",
            eqeqeq: ["error", "always"],
            "function-call-argument-newline": ["warn", "consistent"],
            "lines-between-class-members": "off",
            "max-len": [
                "error",
                {
                    code: 120,
                    tabWidth: 4,
                },
            ],
            "no-extra-parens": "warn",
            "no-magic-numbers": "off",
            "no-underscore-dangle": "off",
            "no-var": "error",
            "nonblock-statement-body-position": ["warn", "beside"],
            "object-curly-spacing": "off",
            "one-var": ["error", "never"],
            "padded-blocks": "off",
            "prefer-const": "off",
            semi: ["error", "always"],
            "sort-imports": "off",
            "space-before-function-paren": "off",
            // https://github.com/typescript-eslint/typescript-eslint/issues/1845
            "@typescript-eslint/explicit-function-return-type": [
                "warn",
                {
                    allowExpressions: true,
                    allowTypedFunctionExpressions: true,
                    allowHigherOrderFunctions: true,
                },
            ],
            "@typescript-eslint/no-inferrable-types": "off",
            "@typescript-eslint/no-unused-vars": [
                "warn",
                {
                    vars: "all",
                    args: "none",
                    ignoreRestSiblings: false,
                    varsIgnorePattern: "^_$",
                },
            ],
        },
    },
];
