# README

This is some basic web dev skeleton.

## Technology Stack in Use

### Backend

- [Node](https://nodejs.org/en)
- [Docker](https://www.docker.com/)/[Podman](https://podman.io/)
- [nginx](https://www.nginx.com/)
- [PostgreSQL](https://www.postgresql.org/)
- Libraries
  - [Fastify](https://www.fastify.io/)
  - [Drizzle ORM](https://orm.drizzle.team/)

### Frontend

- HTML
- CSS

### Both

- JavaScript/ECMAScript
- [TypeScript](https://www.typescriptlang.org/)

### Dev Tools

- [Yarn](https://yarnpkg.com/)
- [ESLint](https://eslint.org/)
- [Rollup.js](https://rollupjs.org/)
- [PostCSS](https://postcss.org/)
- [EditorConfig](https://editorconfig.org/)
- [Git](https://git-scm.com/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [Gitlab](https://gitlab.com/)
  - [GitLab CI](https://docs.gitlab.com/ee/ci/)
  - [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- Node Packages
  - [concurrently](https://www.npmjs.com/package/concurrently)
  - [copy-and-watch](https://www.npmjs.com/package/copy-and-watch)
  - [rimraf](https://www.npmjs.com/package/rimraf)
  - [nodemon](https://www.npmjs.com/package/nodemon)
