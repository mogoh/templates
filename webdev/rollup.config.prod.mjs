import typescript from "@rollup/plugin-typescript";
import terser from "@rollup/plugin-terser";
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import json from "@rollup/plugin-json";


const config = {
    frontend: {
        input: "src/client/js/index.ts",
        output: {
            file: "dist/client/js/index.js",
            format: "esm",
            sourcemap: false,
            compact: true,
            minifyInternalExports: true,
        },
        plugins: [
            typescript(),
            terser(),
            nodeResolve({ preferBuiltins: false }),
            commonjs(),
            json(),
        ],
    },
    backend: {
        input: "src/server/server.ts",
        output: {
            file: "dist/server/server.js",
            format: "esm",
            sourcemap: false,
        },
        plugins: [
            typescript(),
        ],
    },
};

const PART = process.env.PART;

export default config[PART];
