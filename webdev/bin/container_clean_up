#!/usr/bin/env bash

if [[ -z "${IMAGE_NAME}" ]]; then
    echo "ERROR: Environemnt variable IMAGE_NAME is not set."
    exit
fi
if [[ -z "${CONTAINER_TOOL}" ]]; then
    echo "ERROR: Environemnt variable CONTAINER_TOOL is not set."
    exit
fi

# Stop all containers
"${CONTAINER_TOOL}" stop $("${CONTAINER_TOOL}" ps --all --quiet)

# Stop all pods
"${CONTAINER_TOOL}" pod stop --all

# Remove all pods
"${CONTAINER_TOOL}" pod rm --all

# Remove containers belonging to IMAGE_NAME
"${CONTAINER_TOOL}" rm $("${CONTAINER_TOOL}" ps --all --filter "ancestor=${IMAGE_NAME}" --quiet)

# Remove all images belonging to IMAGE_NAME
"${CONTAINER_TOOL}" rmi $("${CONTAINER_TOOL}" images --filter "reference=${IMAGE_NAME}" --quiet)

# Remove all dangling images
"${CONTAINER_TOOL}" rmi $("${CONTAINER_TOOL}" images --filter "dangling=true" --quiet)

# Remove all networks
"${CONTAINER_TOOL}" network prune --force

# Remove all volumes
"${CONTAINER_TOOL}" volume prune --force
