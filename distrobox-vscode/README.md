# README

See also: https://github.com/89luca89/distrobox/blob/main/docs/posts/integrate_vscode_distrobox.md

## Setup .env

```bash
cp .env.example .env
editor .env
```

## Distrobox

### Install Distrobox

https://github.com/89luca89/distrobox

### Create the container

```bash
. .env
mkdir ${WORKING_DIR}/.home

# append .bashrc
cat <<EOF >> ${WORKING_DIR}/.home/.bashrc
PATH=\$HOME/.yarn/bin:\$PATH
XDG_DATA_DIRS=\$HOME/.local/share
EOF

distrobox create --image docker.io/library/archlinux:latest --name ${CONTAINER_NAME} --home ${WORKING_DIR}/.home
```

### In the container

```bash
distrobox enter ${CONTAINER_NAME}
# do container stuff ...
```

### Delete the container

```bash
podman stop $CONTAINER_NAME
distrobox rm --rm-home $CONTAINER_NAME
```

## VS Code

### Fedora Silverblue: Layer VS Code

```bash
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
rpm-ostree refresh-md
rpm-ostree install code
```

### Install Dev Containers

Install: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers

Settings: `"dev.containers.dockerPath": "/usr/bin/podman"`

- See: https://code.visualstudio.com/docs/devcontainers/containers
- See: https://github.com/89luca89/distrobox/blob/main/docs/posts/integrate_vscode_distrobox.md

### Create VS Code Dev Container Config
```bash
#!/usr/bin/env bash

. .env
mkdir ${HOME}/.config/Code/User/globalStorage/ms-vscode-remote.remote-containers/nameConfigs/
cat <<EOF > ${HOME}/.config/Code/User/globalStorage/ms-vscode-remote.remote-containers/nameConfigs/${CONTAINER_NAME}.json
{
    "name" : "${CONTAINER_NAME}",
    "workspaceFolder": "${WORKING_DIR}",
    "remoteUser": "\${localEnv:USER}",
    "settings": {
        "remote.containers.copyGitConfig": false,
        "remote.containers.gitCredentialHelperConfigLocation": "none",
        "terminal.integrated.profiles.linux": {
            "shell": {
                "path": "\${localEnv:SHELL}",
                "args": [
                    "-l"
                ]
            }
        },
        "terminal.integrated.defaultProfile.linux": "shell"
    },
    "remoteEnv": {
        "COLORTERM": "\${localEnv:COLORTERM}",
        "DBUS_SESSION_BUS_ADDRESS": "\${localEnv:DBUS_SESSION_BUS_ADDRESS}",
        "DESKTOP_SESSION": "\${localEnv:DESKTOP_SESSION}",
        "DISPLAY": "\${localEnv:DISPLAY}",
        "LANG": "\${localEnv:LANG}",
        "SHELL": "\${localEnv:SHELL}",
        "SSH_AUTH_SOCK": "\${localEnv:SSH_AUTH_SOCK}",
        "TERM": "\${localEnv:TERM}",
        "VTE_VERSION": "\${localEnv:VTE_VERSION}",
        "XDG_CURRENT_DESKTOP": "\${localEnv:XDG_CURRENT_DESKTOP}",
        "XDG_DATA_DIRS": "\${localEnv:XDG_DATA_DIRS}",
        "XDG_MENU_PREFIX": "\${localEnv:XDG_MENU_PREFIX}",
        "XDG_RUNTIME_DIR": "\${localEnv:XDG_RUNTIME_DIR}",
        "XDG_SESSION_DESKTOP": "\${localEnv:XDG_SESSION_DESKTOP}",
        "XDG_SESSION_TYPE": "\${localEnv:XDG_SESSION_TYPE}"
    }
}
EOF
```

### Run VS Code attached to the container

See also: https://github.com/microsoft/vscode-remote-release/issues/5278

```python
#!/usr/bin/env python

from subprocess import run
from os import environ


WORKING_DIR = environ.get('WORKING_DIR')
if WORKING_DIR is None:
    exit("WORKING_DIR not set.")
CONTAINER_NAME = environ.get('CONTAINER_NAME')
if CONTAINER_NAME is None:
    exit("CONTAINER_NAME not set.")

container_name_hex = CONTAINER_NAME.encode("utf-8").hex()

run([
    "code",
    "--folder-uri",
    f"vscode-remote://attached-container+{container_name_hex}{WORKING_DIR}",
])
```

Requires `xxd`. `xxd` might be part of `vim-common`.

```bash
#!/usr/bin/env bash

code --folder-uri vscode-remote://attached-container+$(printf "${CONTAINER_NAME}" | xxd -p)${WORKING_DIR}
```
