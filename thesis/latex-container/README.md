## build image

```
podman build --file ./Containerfile --tag latex .
```

## create home

```
rm -rf "${HOME}/.containers/latex/home"
mkdir -p "${HOME}/.containers/latex/home"
```

## create container

```
distrobox create --image latex --name latex --home "${HOME}/.containers/latex/home"
```

## enter container

```
distrobox enter latex
```

## remove container and image

```
distrobox stop latex
distrobox rm --force latex
podman stop latex
podman rm latex
podman rmi "$(podman images latex --all --quiet)"
rm -rf "${HOME}"/.containers/latex/home
```