# README

This is a template for writing theses at the department of computer science at the TU Dortmund.
This is a non-official document.
Even though most theses here are written in german the template is partly in english.
Goal is to provide a template usable for german and english language as well.

## Installing

You need, of course, LaTeX for using this.

The “I don’t care, just install” approach:

### Ubuntu/Debian

```
sudo apt install texlive-full python3-pygments
```

### Fedora

```
sudo dnf install texlive-scheme-full python-pygments
```

(This can take some time.)

### Syntax Highlighting

For syntax-highlighting in code examples we use [minted](https://github.com/gpoore/minted).
This needs `-shell-escape` as a parameter for `pdflatex`/`latexmk`.
Minted uses the Python syntax highlighter [Pygments](https://pygments.org/).
You can install pygments via your system package manager or via pip.

```
pip3 install --user pygments
```

## Contribute

Feel free to create a merge request.
Anyone who does not vandalize something will get developer-rights.
This is a trusting approach.

Feel free to expand the examples, correct some typing errors, reorganize code and expand.
If you wish to add a LaTeX library please proved an example usage.
We do not want unused libraries that could come in handy later.
Also keep things simple.

## To Do

* Algorithms/Minted
  * Color box
  * List of listings
  * remove algorithms
  * Include code
  * Pseudocode example
  * List of theorems

* Support epub/mobile-friendly pdf

## License

This work is licensed under the Creative Commons 0 1.0 Universal license.
This means you can do almost anything you want.
See `CC0` for details.

## Contributers

- Leonhard Küper
- Christoph “Hammy” Stahl

## Based on and inspired by the works of

- Maximilian Nöthe [TUDoBeamerTheme](https://github.com/maxnoe/TUDoBeamerTheme), [tudothesis](https://github.com/maxnoe/tudothesis)
- [TU Dortmund FI AbschlussarbeitenAbschlussarbeiten LaTeX Vorlage](https://www.cs.tu-dortmund.de/nps/de/Studium/besondere_Lehrveranstaltungen/Abschlussarbeiten/index.html)